/**
 * INSPINIA - Responsive Admin Theme
 *
 */
(function () {
    angular.module('inspinia', [
        'ngResource',
        'ngAnimate',
        'ui.router',                    // Routing
        'ui.bootstrap',              // Bootstrap
        'toaster'

    ])
})();