/**
 * INSPINIA - Responsive Admin Theme
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written stat for all view in theme.
 *
 */

angular
    .module('inspinia')
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider) {

        $urlRouterProvider.otherwise("/client/products");


        $stateProvider
            .state('client', {
                abstract: true,
                url: "/client",
                templateUrl: "views/common/content.html",
            })
            .state('client.products', {
                url: "/products",
                templateUrl: "views/products/list.html",
                data: { pageTitle: 'Products' },
                controller: 'ProductListCtrl'
            })
            .state('client.product', {
                url: "/products/:product_id",
                templateUrl: 'views/products/details.html',
                controller: 'ProductDetailsCtrl'
            })
            .state('client.subscribers', {
                url: "/subscribers",
                templateUrl: "views/subscribers/list.html",
                data: { pageTitle: 'Subscribers' }
            })
            .state('client.documents', {
                url: "/documents",
                templateUrl: "views/documents/list.html",
                data: { pageTitle: 'Documents' }
            })
            .state('login', {
                url: "/login",
                templateUrl: "views/common/login.html",
                data: { pageTitle: 'Login', specialClass: 'gray-bg' }
            });

        $httpProvider.interceptors.push(function($location, toaster, $spinner, $q) {
            return {
                'request': function(config) {
                    $spinner.active_requests++;
                    var token = localStorage.getItem("auth_token");
                    if(token != null && token != undefined){
                        config.headers.Authorization = "token " + token;
                    }
                    return config;
                },
                'response' : function(response){
                    $spinner.active_requests--;
                    return response;
                },
                'responseError': function(response) {
                    $spinner.active_requests--;
                    if(response.status == 400){
                        for(var index in response.data) {
                            var errors = response.data[index];
                            var error_keyword = "Whoops!";
                            if(index != "non_field_errors"){
                                error_keyword = index;
                            }
                            var error_message = errors.join(', ');
                            toaster.pop({
                                type: 'error',
                                title: error_keyword,
                                body: error_message,
                                showCloseButton: true,
                                timeout: 4000
                            });
                            break;
                        }
                    }
                    else if(response.status == 401){
                        $location.path("login");
                        toaster.pop({
                            type: 'error',
                            title: 'Whoops!',
                            body: 'It looks like You need to login to proceed.',
                            showCloseButton: true,
                            timeout: 4000
                        });
                    }
                    else if(response.status == 403){
                        toaster.pop({
                            type: 'error',
                            title: 'Permission denied',
                            body: response.data.detail,
                            showCloseButton: true,
                            timeout: 4000
                        });
                    }
                    return $q.reject(response);
                }
            };
        });

    }])
    .config(['$resourceProvider', function($resourceProvider) {
        // Don't strip trailing slashes from calculated URLs
        $resourceProvider.defaults.stripTrailingSlashes = false;
    }])
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    })
    .constant('$config', {
        appVersion: 0.01,
        url: function(relative_url){
            if(relative_url.slice(-1) != "/"){
                relative_url += "/";
            }
            return 'http://karagiorgis.pl:8080/' + relative_url
        }
    })
    .constant('$spinner', {
        active_requests: 0
    });