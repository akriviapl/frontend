angular
    .module('inspinia')
    .controller('UserMiniatureCtrl',
        function($scope, User) {

            $scope.profile = User.profile;
        }
    );