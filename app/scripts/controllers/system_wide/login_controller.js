angular
    .module('inspinia')
    .controller('LoginCtrl', function($scope, $location, User, toaster) {

       User.logOut();

        $scope.login_data = {
            "username": "kosto",
            "password": "123123"
        };

        $scope.login = function(){
            User.logIn(
                $scope.login_data,
                {
                    success: function(response){
                        $location.path('client.products');
                        toaster.pop({
                            type: 'success',
                            title: 'Hello!',
                            body: 'Logged in as ' + $scope.login_data.username,
                            showCloseButton: true,
                            timeout: 4000
                        });
                    }

                }
            )
        }
    });

