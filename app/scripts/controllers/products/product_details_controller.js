angular
    .module('inspinia')
    .controller('ProductDetailsCtrl', function($stateParams, $scope, $resource, $config, toaster) {
        $scope.product = $resource(
            $config.url('client/products/' + $stateParams.product_id),
            {},
            {
                "save": {
                    method: 'PATCH'
                }
            }).get();

        $scope.save = function(){
            $scope.product.$save(function(a){
                toaster.pop({
                    type: 'success',
                    title: 'Wohoo!',
                    body: $scope.product.name + ' was correctly saved',
                    showCloseButton: true,
                    timeout: 4000
                });
            })
        };

    });