angular
    .module('inspinia')
    .controller('ProductListCtrl', ['$scope', '$resource', '$config', function($scope, $resource, $config) {
            $scope.products = $resource($config.url('client/products')).query();

            $scope.refresh = function(){
                $scope.products = $resource($config.url('client/products')).query();
            }
        }]
    );