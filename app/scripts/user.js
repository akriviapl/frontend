angular
    .module('inspinia')
    /**
     * Represents currently logged in user
     */
    .factory('User', ['$http', '$resource', '$config', function($http, $resource, $config) {

        var self = this;

        self.profile = JSON.parse(localStorage.getItem("profile"));


        self._getToken = function(){
            self.token = localStorage.getItem("auth_token");
        };

        self._setToken = function(token){
            self.token = localStorage.setItem("auth_token", token);
            self.token = token;
        };

        self.token = "";
        self._getToken();


        self.hasSavedToken = function(){
            return self.token != "";
        };

        self.logIn = function(login_data, callbacks){
            $http.post(
                $config.url('auth/login'),
                login_data
                )
                .then(
                    function(response){

                        self._setToken(
                            response.data.auth_token
                        );
                        self.profile = $resource($config.url('client/profile')).get({}, function(data){
                            localStorage.setItem("profile", JSON.stringify(data));
                        });

                        if(callbacks.success){
                            callbacks.success(response);
                        }
                    },
                    callbacks.error
                );
        };

        self.logOut = function(){
            localStorage.removeItem("auth_token");
            localStorage.removeItem("profile");
        };

        return self;
    }]);
