## Recordo frontend

### Project setup

##### Requirements

* nodejs
* npm
* bower

*Note: having `npm`, You can easily install bower globally using `npm install -g bower`*

##### Setup
*Firstly, You need to install all development dependencies including Grunt and Bower*
`npm install`
*Then, You need to install runtime dependencies using bower*
`bower install`
*You are now ready to launch grunt live server*
`node_modules/grunt-cli/bin/grunt live`

*Development server will start on localhost:9000*